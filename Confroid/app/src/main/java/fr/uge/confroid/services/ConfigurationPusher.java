package fr.uge.confroid.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import java.util.Date;

import fr.uge.confroid.models.Configuration;
import fr.uge.confroid.utils.DatabaseHandler;

public class ConfigurationPusher extends Service {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("LocalService", "Received start id " + startId + ": " + intent);

      try{
          DatabaseHandler handler= new DatabaseHandler(this.getApplicationContext());

          String name=intent.getStringExtra("name");
          String version=intent.getStringExtra("version");
          String tag=intent.getStringExtra("tag");
          String content=intent.getStringExtra("content");
          String token=intent.getStringExtra("token");

          if(name==null || version==null || token==null || content==null || token==null){
              throw  new Exception("Error");
          }

          tag= tag!=null ? tag: "";

          Configuration config = new Configuration();
          config.setName(name);
          config.setVersion(version);
          config.setTag(tag);
          config.setContent(content);

          handler.addConfiguration(config, token);


      }catch (Exception e){

      }
        return START_NOT_STICKY;
    }

}
