package fr.uge.confroid.recycler;

import android.content.Intent;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import com.example.confroid.R;
import fr.uge.confroid.application.Message;


import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class ModifiActivity extends AppCompatActivity {

    LinearLayout linearLayout;
    ScrollView scrollView;
    Object conf;


    public void boucle(Object classe) {

        Field[] getfields = classe.getClass().getDeclaredFields();

        for (int i = 0; i < getfields.length; i++) {
            try {
                conf = getfields[i].get(classe);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            dynamiclayout(getfields[i], conf);
        }
    }

    public void dynamiclayout(Field field, Object value) {
        String type;


        RecyclerView recy = new RecyclerView(this);
        EditText editText = new EditText(this);
        CheckBox bool = new CheckBox(this);
        TextView newAppli = new TextView(this);
        Button go = new Button(this);

        TextView texte = new TextView(this);


        type = field.getType().toString();
        if (type.equals("int") || type.equals("long") || type.equals("double") || type.equals("float") || type.equals("class java.lang.String")) {

            //nom champ
            texte.setText(field.getName());

            //valeur
            editText.setHint(field.getName());
            editText.setText(value.toString());

            linearLayout.addView(texte);
            linearLayout.addView(editText);
            return;

        }

        if (type.equals("boolean")) {
            //nom champ
            texte.setText(field.getName());
            //valeur
            bool.setChecked((Boolean) value);
            linearLayout.addView(texte);
            linearLayout.addView(bool);
            return;


        }

        //Autre Class
        texte.setText(field.getName());
        newAppli.setText(value.toString());
        String name = "Param  Class" + value;
        go.setText(name);
        go.setOnClickListener(x -> {
            go.setEnabled(false);
            Toast.makeText(this, "Modifier", Toast.LENGTH_SHORT).show();
            boucle(value);


        });

        linearLayout.addView(texte);
        linearLayout.addView(newAppli);
        linearLayout.addView(go);


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modifi);

        //Layout dynamic
        setContentView(R.layout.dynamic_layout);

        //Pour scroler lelinear layout
        scrollView = new ScrollView(this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        scrollView.setLayoutParams(layoutParams);
        linearLayout = new LinearLayout(this);
        LinearLayout.LayoutParams linearParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setLayoutParams(linearParams);
        scrollView.addView(linearLayout);


        Intent intent = getIntent();
        //TODO recupere configuration
        int val = intent.getIntExtra("position", 0);
        Object conf;
        List<Integer> value  = new ArrayList<>();
        value.add(val);
        value.add(2);
        value.add(3);
        if (val % 2 == 0) {
            conf = new fr.uge.confroid.application.Calculette(val, true, 12, new Message("nv classe", false, value));
        } else {
            conf = new fr.uge.confroid.application.Message("Votre message", false,value);
        }


        boucle(conf);

        //Ajoute au layout tout les éléments a scroller
        LinearLayout linearLayout1 = findViewById(R.id.linear_layout);
        if (linearLayout1 != null) {
            linearLayout1.addView(scrollView);
        }
    }
}