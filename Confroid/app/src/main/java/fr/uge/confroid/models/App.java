package fr.uge.confroid.models;

public class App {

    private  int id;
    private  String name;
    private String packageName;
    private String token;

    public App(int id, String name, String packageName, String token) {
        this.id = id;
        this.name = name;
        this.packageName = packageName;
        this.token = token;
    }

    public App(String name, String packageName, String token) {
        this.id=0;
        this.name = name;
        this.packageName = packageName;
        this.token = token;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    public String getToken() {
        return token;
    }
}
