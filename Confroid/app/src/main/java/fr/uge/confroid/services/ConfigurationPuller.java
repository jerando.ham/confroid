package fr.uge.confroid.services;

import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import fr.uge.confroid.models.App;
import fr.uge.confroid.models.Configuration;
import fr.uge.confroid.utils.DatabaseHandler;

public class ConfigurationPuller extends Service {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("LocalService", "Received start id " + startId + ": " + intent);

        try{
            DatabaseHandler handler= new DatabaseHandler(this.getApplicationContext());
            String name=intent.getStringExtra("name");
            String version=intent.getStringExtra("version");
            String token=intent.getStringExtra("token");
            String service= intent.getStringExtra("receiver");
            String requestId = intent.getStringExtra("requestId");

            App app= handler.getApp(token);

            if(name==null || version==null || token==null || service==null || requestId==null){
                throw  new Exception("Error");
            }

            if(app!=null){
                Configuration config= handler.getConfiguration(name,version);

                if(config!=null){
                    Intent serviceIntent= new Intent();
                    serviceIntent.putExtra("name",config.getName());
                    serviceIntent.putExtra("version",config.getVersion());
                    serviceIntent.putExtra("content",config.getContent());
                    serviceIntent.putExtra("requestId",requestId);
                    serviceIntent.setComponent(new ComponentName(app.getPackageName(), service));

                    startService(serviceIntent);
                }
            }


        }catch (Exception e){

        }
        return START_NOT_STICKY;
    }

}
