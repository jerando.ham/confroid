package fr.uge.confroid.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import fr.uge.confroid.models.Configuration;

import androidx.annotation.Nullable;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.uge.confroid.models.App;

public class DatabaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "confroidManager";

    static class Application{
        public static String TABLE_NAME = "application";
        public static String TABLE_ID = "id";
        public static String APPLICATION_NAME = "name";
        public static String PACKAGE_NAME = "packageName";
        public static String TOKEN = "token";
    }

    static class Config{
        public static String TABLE_NAME = "configuration";
        public static String CONFIGURATION_ID = "id";
        public static String CONFIGURATION_NAME = "name";
        public static String CONFIGURATION_VERSION = "version";
        public static String CONFIGURATION_TAG = "tag";
        public static String CONTENT = "content";
        public static String CREATED = "createdAt";
        public static String APP_ID = "appId";
    }

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        //3rd argument to be passed is CursorFactory instance
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_APPLICATION_TABLE = "CREATE TABLE " + Application.TABLE_NAME + "("
                + Application.TABLE_ID + " INTEGER PRIMARY KEY," + Application.APPLICATION_NAME + " TEXT,"
                + Application.PACKAGE_NAME+ " TEXT," +Application.TOKEN+ " TEXT)";

        String CREATE_CONFIGURATION_TABLE = "CREATE TABLE " + Config.TABLE_NAME + "("
                + Config.CONFIGURATION_ID + " INTEGER PRIMARY KEY," + Config.CONFIGURATION_NAME + " TEXT,"
                + Config.CONFIGURATION_VERSION+ " TEXT," +Config.CONFIGURATION_TAG+ " TEXT,"
                + Config.CONTENT+" TEXT,"+ Config.CREATED+" DATE,"+Config.APP_ID+" INTEGER)";

        db.execSQL(CREATE_APPLICATION_TABLE);

        db.execSQL(CREATE_CONFIGURATION_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {

        db.execSQL("DROP TABLE IF EXISTS " + Application.TABLE_NAME);

        db.execSQL("DROP TABLE IF EXISTS " + Config.TABLE_NAME);
    }

    public void addApplication(String name,String packageName,String token){

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Application.APPLICATION_NAME, name); // Application Name
        values.put(Application.PACKAGE_NAME, packageName); // Package name
        values.put(Application.TOKEN, token); // Package name

        // Inserting Row
        db.insert(Application.TABLE_NAME, null, values);
        //2nd argument is String containing nullColumnHack
        db.close(); // Closing database connection
    }

    public App getApp(String token){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(Application.TABLE_NAME, new String[] {Application.TABLE_ID, Application.APPLICATION_NAME,
                        Application.PACKAGE_NAME, Application.TOKEN }, Application.TOKEN + "=?",
                new String[] { token }, null, null, null, null);
        if (cursor != null){
            cursor.moveToFirst();

            App app= new App(Integer.parseInt(cursor.getString(0)),
                    cursor.getString(1),
                    cursor.getString(2),cursor.getString(3));

            return app;
        }
            return null;


    }


    public void addConfiguration(Configuration config,String token){

        SQLiteDatabase db = this.getWritableDatabase();

        App app= this.getApp(token);

        if(app==null) return;

        ContentValues values = new ContentValues();
        values.put(Config.CONFIGURATION_NAME, config.getName());
        values.put(Config.CONFIGURATION_VERSION, config.getVersion());
        values.put(Config.CONFIGURATION_TAG, config.getTag());
        values.put(Config.CONTENT, config.getContent());
        values.put(Config.APP_ID, app.getId());


        // Inserting Row
        db.insert(Config.TABLE_NAME, null, values);
        //2nd argument is String containing nullColumnHack
        db.close(); // Closing database connection
    }

    public List<Configuration> getConfigurations(String token){

        App app= this.getApp(token);

        if(app==null) return null;

        ArrayList<Configuration> configurations= new ArrayList<>();

        String selectQuery = "SELECT  * FROM " + Config.TABLE_NAME+ " WHERE "+Config.APP_ID+"="+app.getId();

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Configuration config = new Configuration();

                config.setId(cursor.getInt(0));
                config.setName(cursor.getString(1));
                config.setVersion(cursor.getString(2));
                config.setTag(cursor.getString(3));
                config.setContent(cursor.getString(4));

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date d=new Date();
                try{
                    d=  dateFormat.parse(cursor.getString(5));
                }catch (Exception e){

                }

                config.setCreatedAt(d);
                // Adding contact to list
                configurations.add(config);
            } while (cursor.moveToNext());
        }

        return configurations;
    }

    public Configuration getConfiguration(String name){
        Configuration config= new Configuration();

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(Config.TABLE_NAME, new String[] {Config.CONFIGURATION_ID, Config.CONFIGURATION_NAME,
                        Config.CONFIGURATION_VERSION, Config.CONFIGURATION_TAG,Config.CONTENT,Config.CREATED }, Config.CONFIGURATION_NAME + "=?",
                new String[] { name }, null, null, null, null);
        if (cursor != null){
            cursor.moveToFirst();


            config.setId(cursor.getInt(0));
            config.setName(cursor.getString(1));
            config.setVersion(cursor.getString(2));
            config.setTag(cursor.getString(3));
            config.setContent(cursor.getString(4));

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date d=new Date();
            try{
                d=  dateFormat.parse(cursor.getString(5));
            }catch (Exception e){

            }

            config.setCreatedAt(d);
            return  config;
        }
        return null;

    }

    public Configuration getConfiguration(String name,String version){
        Configuration config= new Configuration();

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(Config.TABLE_NAME, new String[] {Config.CONFIGURATION_ID, Config.CONFIGURATION_NAME,
                        Config.CONFIGURATION_VERSION, Config.CONFIGURATION_TAG,Config.CONTENT,Config.CREATED }, Config.CONFIGURATION_NAME + "=? AND "+Config.CONFIGURATION_VERSION+"= ?",
                new String[] { name,version }, null, null, null, null);
        if (cursor != null){
            cursor.moveToFirst();


            config.setId(cursor.getInt(0));
            config.setName(cursor.getString(1));
            config.setVersion(cursor.getString(2));
            config.setTag(cursor.getString(3));
            config.setContent(cursor.getString(4));

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date d=new Date();
            try{
                d=  dateFormat.parse(cursor.getString(5));
            }catch (Exception e){

            }

            config.setCreatedAt(d);
            return  config;
        }
        return null;

    }

    public void updateConfiguration(Configuration config){


    }

    public List<App> getApps(){

        ArrayList<App> apps= new ArrayList<>();

        String selectQuery = "SELECT  * FROM " + Application.TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {

                App app= new App(Integer.parseInt(cursor.getString(0)),
                        cursor.getString(1),
                        cursor.getString(2),cursor.getString(3));
                apps.add(app);
            } while (cursor.moveToNext());
        }
        return apps;
    }
}
