package fr.uge.confroid.receivers;

import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import java.util.UUID;

import androidx.annotation.Nullable;

import fr.uge.confroid.utils.DatabaseHandler;

public class TokenDispenser extends Service {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("LocalService", "Received start id " + startId + ": " + intent);

        try{
            DatabaseHandler handler= new DatabaseHandler(this.getApplicationContext());

            String appName=intent.getStringExtra("appName");
            String packageName=intent.getStringExtra("packageName");

            String service= intent.getStringExtra("receiver");


            if(appName==null || packageName==null || service==null ){
                throw  new Exception("Error");
            }
            String uuid = UUID.randomUUID().toString();

            uuid= uuid.replace("-", "");
            String token=uuid.substring(0,12);
            handler.addApplication(appName,packageName,token);

            Intent serviceIntent= new Intent();
            serviceIntent.putExtra("token",token);
            serviceIntent.setComponent(new ComponentName(packageName, service));
            startService(serviceIntent);

            Log.e("Name", appName);
            Log.e("Package", packageName);
            Log.e("Service", service);

        }catch (Exception e){
            Log.e("Error","Null params");
        }

        return START_NOT_STICKY;
    }
}
