package fr.uge.confroid.application;

import java.util.List;

public class Message {

    public String msg;
    public boolean b;
    public List<Integer> val;

    public Message(){

    }

    public Message(String msg, boolean b, List<Integer> val) {

        this.msg = msg;
        this.b = b;
        this.val = val;
    }
}
