package fr.uge.confroid.recycler;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.confroid.ConfigurationActivity;
import com.example.confroid.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import fr.uge.confroid.models.Configuration;

public class ConfigurationAdapter extends RecyclerView.Adapter<ConfigurationAdapter.AdapterViewHolder> {

    private ArrayList<Configuration> entries;
    private LayoutInflater inflater;
    private Context context;
    private Button btn_export;
    private static final int PICKFILE_REQUEST_CODE = 10;

    public static class AdapterViewHolder extends RecyclerView.ViewHolder {
        //Card quit contient le titre et la description
        public CardView cardView;
        public TextView name;
        public TextView version;
        public TextView createdAt;
        public Button   button;

        public void updateItem(Configuration entry){
            name.setText(entry.getName());
            version.setText(entry.getVersion());
            //createdAt.setText(entry.getCreatedAt().toString());
            createdAt.setText("12/10/2020");
        }
        public AdapterViewHolder(View itemView) {
            super(itemView);
            cardView= itemView.findViewById(R.id.cardView);
            name= itemView.findViewById(R.id.configName);
            version= itemView.findViewById(R.id.configVersion);
            createdAt= itemView.findViewById(R.id.configDate);
            button = itemView.findViewById(R.id.ExportBtn);



        }
    }

    public ConfigurationAdapter(Context context,ArrayList<Configuration> entries) {
        this.context=context;
        this.inflater = LayoutInflater.from(context);
        this.entries = entries;
    }
    @NonNull
    @Override
    public AdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.from(parent.getContext()).inflate(R.layout.configuration_item_layout, parent, false);

        return new AdapterViewHolder(view);
    }



    @Override
    public void onBindViewHolder(@NonNull AdapterViewHolder holder, int position) {

        Configuration entry= entries.get(position);

        holder.updateItem(entry);

        holder.cardView.setOnClickListener(view -> {

            Intent intent = new Intent(this.context, ModifiActivity.class);

            intent.putExtra("CONFIG_ID", entry.getId());
            this.context.startActivity(intent);
        });


//        holder.button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent chooseFile;
//                Intent intent;
//                chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
//                chooseFile.addCategory(Intent.CATEGORY_OPENABLE);
//                chooseFile.setType("*/*");
//                intent = Intent.createChooser(chooseFile, "Choose a file");
//                ((Activity) context).startActivityForResult(intent, PICKFILE_REQUEST_CODE);
//
//            }
//
//
//        });
        holder.button.setOnClickListener(view -> {
            String content = entry.getContent();
            saveToFile(content,context);
            try {
                encrypt();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (NoSuchPaddingException e) {
                e.printStackTrace();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            }

      });
    }

   public void encrypt() throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {
        // Here you read the cleartext.
        FileInputStream fis = new FileInputStream("/data/data/com.example.confroid/files/content.xml");
        // This stream write the encrypted text. This stream will be wrapped by another stream.
        FileOutputStream fos = new FileOutputStream("/data/data/com.example.confroid/files/contentcrypto.xml");
       SecretKeySpec skeySpec = new SecretKeySpec("MyDifficultPassw".getBytes(), "AES");
       CipherOutputStream cos;
       Cipher cipher1 = Cipher.getInstance("AES");
       cipher1.init(Cipher.ENCRYPT_MODE, skeySpec);
       cos = new CipherOutputStream(fos, cipher1);
// Here you read from the file in fis and write to cos.
       byte[] b = new byte[8];
       int i = fis.read(b);
       while (i != -1) {
           cos.write(b, 0, i);
           i = fis.read(b);
       }
       cos.flush();
    }






    @Override
    public int getItemCount() {
        return entries.size();
    }



    public void saveToFile(String content,Context ctx) {

        FileOutputStream fos = null;
        try {
            fos = ctx.openFileOutput("content.xml", ctx.MODE_PRIVATE);
            fos.write(content.getBytes());
            Toast.makeText(ctx, "Saved to " + ctx.getFilesDir() + "/content.xml",
                    Toast.LENGTH_LONG).show();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
