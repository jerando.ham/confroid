package fr.uge.confroid.utils;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;


import com.thoughtworks.xstream.XStream;

import org.w3c.dom.Document;

import java.io.ByteArrayOutputStream;
import java.util.UUID;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import fr.uge.confroid.models.App;

public class ConfroidUtils  {

   public static void saveConfiguration(Context context, String name, Object value, String versionName,String tag,String receiver,String token){

       XStream xstream = new XStream();

       String content= xstream.toXML(value);

       Intent serviceIntent= new Intent();
       serviceIntent.setComponent(new ComponentName("com.example.confroid", "fr.uge.confroid.services.ConfigurationPusher"));
       serviceIntent.putExtra("name",name);
       serviceIntent.putExtra("version",versionName);
       serviceIntent.putExtra("tag",tag);
       serviceIntent.putExtra("content",content);
       serviceIntent.putExtra("token",token);

       context.startService(serviceIntent);

   }

    public static String toXml() throws ParserConfigurationException {

        XStream xstream = new XStream();

        DocumentBuilderFactory dbFactory =
                DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.newDocument();

        App app= new App(1, "Application ", "com.example.pack", "Aokk625");
       int a=3;
        return xstream.toXML(a);

    }



    public static void registerApplication(Context context, String appName,  String packageName,String tag,String receiver){

        Intent serviceIntent= new Intent();
        serviceIntent.setComponent(new ComponentName("com.example.confroid", "fr.uge.confroid.receivers.TokenDispenser"));
        serviceIntent.putExtra("appName",appName);
        serviceIntent.putExtra("packageName",packageName);
        serviceIntent.putExtra("receiver",receiver);

        context.startService(serviceIntent);

    }

    public static void loadConfiguration(Context context, String name, String version,String receiver,String token){

        String uuid = UUID.randomUUID().toString();

        uuid= uuid.replace("-", "");
        String requestId=uuid.substring(0,12);

        Intent serviceIntent= new Intent();
        serviceIntent.setComponent(new ComponentName("com.example.confroid", "fr.uge.confroid.services.ConfigurationPuller"));
        serviceIntent.putExtra("name",name);
        serviceIntent.putExtra("version",version);
        serviceIntent.putExtra("requestId",requestId);
        serviceIntent.putExtra("receiver",receiver);
        serviceIntent.putExtra("token",token);

        context.startService(serviceIntent);
    }

}
