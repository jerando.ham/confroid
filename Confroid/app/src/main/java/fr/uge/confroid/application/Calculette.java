package fr.uge.confroid.application;

public class Calculette {

    public int valeur;
    public boolean aBoolean;
    public int otherint;
    public Message msg;

    public Calculette() {

    }

    public int getAz() {
        return otherint;
    }

    public void setAz(int otherint) {
        this.otherint = otherint;
    }

    public Calculette(int valeur, boolean bool, int otherint, Message msg) {
        this.valeur = valeur;
        this.aBoolean = bool;
        this.otherint = otherint;
        this.msg = msg;
    }

    public int getvaleur() {
        return valeur;
    }

    public void setvaleur(int valeur) {
        this.valeur = valeur;
    }

    public boolean isaBoolean() {
        return aBoolean;
    }

    public void setaBoolean(boolean aBoolean) {
        this.aBoolean = aBoolean;
    }
}
