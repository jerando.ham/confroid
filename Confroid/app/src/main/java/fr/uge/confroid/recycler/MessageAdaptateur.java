package fr.uge.confroid.recycler;



import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.confroid.ConfigurationActivity;
import com.example.confroid.R;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import fr.uge.confroid.models.App;

public class MessageAdaptateur extends RecyclerView.Adapter<MessageAdaptateur.ViewHolder> implements Serializable {

    private transient List<App> applications;
    private Context context;

    public class ViewHolder extends RecyclerView.ViewHolder {

        private  TextView appName;
        public CardView cardView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            appName = itemView.findViewById(R.id.texte);
            cardView= itemView.findViewById(R.id.cardView);

        }

        private void update(App app) {

            appName = itemView.findViewById(R.id.texte);
            appName.setText(app.getName());
        }

    }

    public MessageAdaptateur(Context context,List<App> entries) {
        super();
        this.context=context;
        applications = new ArrayList<App>();
        this.applications = entries;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        //return null;
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_message_adaptateur, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        App app= applications.get(i);

        viewHolder.update(app);

        viewHolder.cardView.setOnClickListener(view -> {

            Intent intent = new Intent(this.context.getApplicationContext(), ConfigurationActivity.class);
            intent.putExtra("APP_TOKEN", app.getToken());
            this.context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return applications.size();
    }



}