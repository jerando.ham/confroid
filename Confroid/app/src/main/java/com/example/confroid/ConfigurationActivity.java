package com.example.confroid;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import fr.uge.confroid.models.Configuration;
import fr.uge.confroid.recycler.ConfigurationAdapter;
import fr.uge.confroid.utils.DatabaseHandler;

public class ConfigurationActivity extends AppCompatActivity {

    public RecyclerView recyclerView;

    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private Button btn_export;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration);

        DatabaseHandler handler= new DatabaseHandler(this.getApplicationContext());
        Intent intent = getIntent();
        String token = intent.getStringExtra("APP_TOKEN"); //if it's a string you stored.

        Toast toast=Toast.makeText(getApplicationContext(),"Token: "+token,Toast.LENGTH_SHORT);
        toast.show();


        recyclerView= (RecyclerView)findViewById(R.id.configList);
        btn_export=(Button) findViewById(R.id.ExportBtn);


        List<Configuration> entries=handler.getConfigurations(token);

        mLayoutManager = new LinearLayoutManager(this);




        recyclerView.setLayoutManager(mLayoutManager);
        adapter=new ConfigurationAdapter(this,(ArrayList<Configuration>)entries);

        recyclerView.setAdapter(adapter);
    }
}