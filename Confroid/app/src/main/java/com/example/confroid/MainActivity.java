package com.example.confroid;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.confroid.R;
import com.thoughtworks.xstream.XStream;

import java.util.ArrayList;
import java.util.List;

import fr.uge.confroid.models.App;
import fr.uge.confroid.models.Configuration;
import fr.uge.confroid.recycler.AppliConfroid;
import fr.uge.confroid.recycler.MessageAdaptateur;
import fr.uge.confroid.recycler.ModifiActivity;
import fr.uge.confroid.recycler.RecyInter;
import fr.uge.confroid.utils.ConfroidUtils;
import fr.uge.confroid.utils.DatabaseHandler;

public class MainActivity extends AppCompatActivity  {

    private RecyclerView recyclerView;
    private MessageAdaptateur msgAdaptateur;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setTitle("Appications connectées");

        //db
        DatabaseHandler handler= new DatabaseHandler(this.getApplicationContext());
       /* int NUM_APPS=3;
        int NUM_CONFIGS=3;
        String names[]={"Calculator","Contacts","BNP"};
        String packages[]={"com.example.calculator","com.example.contacts","com.example.bnp"};

        for (int i=0;i<NUM_APPS;i++){

            handler.addApplication(names[i],packages[i],"token"+i);
            String contents[]={"<string>Hellow world</string>","<int-array><int>1</int><int>3</int><int>10</int></int-array>","<org.confroid.project.training.Customer><firstName>John</firstName><lastName>Doe</lastName><id>1</id></org.confroid.project.training.Customer>"};

            for(int j=0;j<NUM_CONFIGS;j++){

                int num=NUM_APPS*i + j;
                Configuration config=new Configuration();
                config.setName("Config "+num);
                config.setVersion("Version "+num);
                config.setTag("Tag "+num);
                config.setContent(contents[j]);

                handler.addConfiguration(config,"token"+i);
            }
        }*/
        List<App> list= handler.getApps();

        recyclerView = findViewById(R.id.recyclerView);
        msgAdaptateur = new MessageAdaptateur(this, (ArrayList<App>) list);

        recyclerView.setAdapter(msgAdaptateur);
        updateLayoutManager(null);

    }


    private void updateLayoutManager(View v) {
        recyclerView.setLayoutManager(createLayoutManager());
    }

    private RecyclerView.LayoutManager createLayoutManager() {
        return new LinearLayoutManager(this);

    }

}
