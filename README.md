
Projet : Confroid 

Membre du groupe: BOULICHE Sofia, BOUZOUITA Mohamed Ali, METAYER Ambre, KIFACK Victorien, JERANDO Hamza, BELLADJEL Ouiza.

Le but du projet: Gestion des configuration des applications grâce à l'application Confroid.

Utilisation: 
- Enregistrement des application dans l'application Confroid.
- Sauvegarde des configurations, exemples: sauvegardes des calculs, contacts,...
- Récupération des anciennes versions de configurations.
- Gestion des version de configurations.
- Gestion des abonnements aux configurations et à l'annulation de ces derniers.

Limitations et Bugs:
- Lancement de l'application Confroid avant les autres applications.

